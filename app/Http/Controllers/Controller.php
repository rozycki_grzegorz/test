<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\HtmlString;
use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Environment;
use League\CommonMark\Extension\Table\TableExtension;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function welcome()
    {
        $environment = Environment::createCommonMarkEnvironment();
        $environment->addExtension(new TableExtension);
        $converter = new CommonMarkConverter([
            'allow_unsafe_links' => false,
        ], $environment);
        $contents = file_get_contents(base_path('README.md'));
        $readme = new HtmlString($converter->convertToHtml($contents));

        return view('welcome', compact('readme'));
    }
}
